import xml.etree.ElementTree as ET
from xmlHelper import *

# Simulation fixed properties

def get_simu_settings_node(root,node_name):
    xpath_expr = "./MolflowSimuSettings/" + node_name
    setting_node = root.find(xpath_expr)
    if setting_node is not None:
        return setting_node
    else:
        print("The '{}' attribute was not found.".format(node_name))


def get_gas_mass(root):
    xpath_expr = "./MolflowSimuSettings/Gas"
    gas_node = root.find(xpath_expr)
    if gas_node is not None and 'mass' in gas_node.attrib:
        return float(gas_node.attrib['mass'])
    else:
        print("The 'mass' attribute was not found.")

def set_gas_mass(root):
    xpath_expr = "./MolflowSimuSettings/Gas"
    gas_node = root.find(xpath_expr)
    if gas_node is not None and 'mass' in gas_node.attrib:
        return float(gas_node.attrib['mass'])
    else:
        print("The 'mass' attribute was not found.")

class ScatteringProperties:
    def __init__(self, enabled: bool, massRatio: float, meanFreePath: float, lowSpeedCutoff_enabled: bool, lowSpeedCutoff_speed: float):
        self.enabled = enabled #scattering on or off
        self.massRatio = massRatio # simulated gas / backround gas
        self.meanFreePath = meanFreePath # cm
        self.lowSpeedCutoff_enabled = lowSpeedCutoff_enabled
        self.lowSpeedCutoff_speed = lowSpeedCutoff_speed # m/s

# Getter for ScatteringProperties
def get_scattering_properties(root):
    scattering_node = root.find("./MolflowSimuSettings/Gas/Scattering")
    if scattering_node is not None:
        enabled = scattering_node.attrib.get('enabled', 'false').lower() == 'true'
        massRatio = float(scattering_node.attrib.get('massRatio', 1.0))
        meanFreePath = float(scattering_node.attrib.get('meanFreePath_cm', 10.0))

        # LowSpeedCutoff node
        low_speed_cutoff_node = scattering_node.find("LowSpeedCutoff")
        if low_speed_cutoff_node is not None:
            lowSpeedCutoff_enabled = low_speed_cutoff_node.attrib.get('enabled', 'false').lower() == 'true'
            lowSpeedCutoff_speed = float(low_speed_cutoff_node.attrib.get('speed', 0.0))
        else:
            lowSpeedCutoff_enabled = False
            lowSpeedCutoff_speed = 0.0

        return ScatteringProperties(enabled, massRatio, meanFreePath, lowSpeedCutoff_enabled, lowSpeedCutoff_speed)
    else:
        print("The 'Scattering' node was not found.")
        return None
    
# Setter for ScatteringProperties
def set_scattering_properties(root, scattering_properties: ScatteringProperties):
    scattering_node = root.find("./MolflowSimuSettings/Gas/Scattering")
    if scattering_node is not None:
        # Set scattering attributes
        scattering_node.attrib['enabled'] = 'true' if scattering_properties.enabled else 'false'
        scattering_node.attrib['massRatio'] = str(scattering_properties.massRatio)
        scattering_node.attrib['meanFreePath_cm'] = str(scattering_properties.meanFreePath)

        # Find or create LowSpeedCutoff node
        low_speed_cutoff_node = scattering_node.find("LowSpeedCutoff")
        if low_speed_cutoff_node is None:
            low_speed_cutoff_node = ET.SubElement(scattering_node, "LowSpeedCutoff")

        # Set LowSpeedCutoff attributes
        low_speed_cutoff_node.attrib['enabled'] = 'true' if scattering_properties.lowSpeedCutoff_enabled else 'false'
        low_speed_cutoff_node.attrib['speed'] = str(scattering_properties.lowSpeedCutoff_speed)
    else:
        print("The 'Scattering' node was not found.")