import xml.etree.ElementTree as ET
from enum import Enum
from typing import List
from xmlHelper import *

# Facet properties

# Look up a facet's node in the geometry XML node.
# This allows to access fixed facet properties
# Facet results (textures, etc.) are stored in a different node
# Facet ID starts from 0 (as opposed to Molflow GUI, where it starts from 1)
def find_facet_node(root, facet_id):
    # Define the XPath expression to find the Facet element
    xpath_expr = "./Geometry/Facets/Facet[@id='{}']".format(facet_id)

    # Find the matching Facet node
    facet_node = root.find(xpath_expr)

    if facet_node is None:
        print("Facet node with id='{}' not found in XML.".format(facet_id))

    return facet_node

def get_facet_count(root):
    # Find the Facets node
    facets_node = root.find("./Geometry/Facets")
    
    if facets_node is None:
        print("Facets node not found.")
        return None
    
    # Determine the next available id by counting existing Vertex elements
    facets_count = len(facets_node.findall("Facet"))
    return facets_count

# Sticking get/set
def get_sticking(facet_node):
    node = get_subnode(facet_node,"Sticking")
    return float(get_node_attribute(node,"constValue"))

def set_sticking(facet_node, new_sticking):
    node = get_subnode(facet_node,"Sticking")
    set_node_attribute(node,"constValue",new_sticking)

# Opacity get/set
def get_opacity(facet_node):
    node = get_subnode(facet_node,"Opacity")
    return float(get_node_attribute(node,"constValue"))

def set_opacity(facet_node, new_opacity):
    node = get_subnode(facet_node,"Opacity")
    set_node_attribute(node,"constValue",new_opacity)

# Two-sidedness
def is_2_sided(facet_node: ET.Element) -> bool:
    node = get_subnode(facet_node,"Opacity")
    return bool(int(get_node_attribute(node,"is2sided")))

def set_2_sided(facet_node: ET.Element, is2sided:bool) -> None:
    node = get_subnode(facet_node,"Opacity")
    set_node_attribute(node, "is2sided", int(is2sided))

# Outgassing get/set
class DesorptionType(Enum):
    No = 0 #Avoid python None keyword
    Uniform = 1
    Cosine = 2
    Cosine_N = 3
    AngleMap = 4 

class Outgassing:
    def __init__(self, value: float, type: DesorptionType, exponent: float):
        self.value = value #Pa.m3/s
        self.type = type
        self.exponent = exponent #For cosine^N

    def __repr__(self):
        return f"Outgassing(value={self.value} Pa.m3/s, type={self.type.name}, exponent={self.exponent})"
    
def get_outgassing(facet_node):
    node = get_subnode(facet_node,"Outgassing")
    value = float(get_node_attribute(node,"constValue"))
    type = DesorptionType(int(get_node_attribute(node,"desType")))
    exponent = float(get_node_attribute(node,"desExponent"))
    return Outgassing(value,type,exponent)

def set_outgassing(facet_node, new_outgassing):
    node = get_subnode(facet_node,"Outgassing")
    set_node_attribute(node,"constValue",new_outgassing.value)
    set_node_attribute(node,"desType",new_outgassing.type.value)
    set_node_attribute(node,"desExponent",new_outgassing.exponent)

def set_outgassing_rate(facet_node, new_rate):
    node = get_subnode(facet_node,"Outgassing")
    set_node_attribute(node,"constValue",new_rate)

def set_outgassing_type(facet_node, new_type):
    node = get_subnode(facet_node,"Outgassing")
    set_node_attribute(node,"constValue",new_type)

# Temperature get/set
def get_temperature(facet_node):
    node = get_subnode(facet_node,"Temperature")
    return float(get_node_attribute(node,"value"))

def set_temperature(facet_node, new_temp):
    node = get_subnode(facet_node,"Temperature")
    set_node_attribute(node,"value",new_temp)

# Structure

def get_structure(facet_node: ET.Element) -> int:
    node = get_subnode(facet_node, "Structure")
    return int(get_node_attribute(node, "inStructure")) #Indexed from 0, -1: all

def set_structure(facet_node: ET.Element, in_structure: int) -> None:  #Indexed from 0, -1: all
    node = get_subnode(facet_node, "Structure")
    set_node_attribute(node, "inStructure", in_structure)

# Link

def get_link(facet_node: ET.Element) -> int:  # Indexed from 1, 0: not a link, -1: go back to previous structure
    node = get_subnode(facet_node, "Structure")
    return int(get_node_attribute(node, "linksTo"))

def set_link(facet_node: ET.Element, links_to: int) -> None:  # Indexed from 1, 0: not a link, -1: go back to previous structure
    node = get_subnode(facet_node, "Structure")
    set_node_attribute(node, "linksTo", links_to)

# Reflection

class Reflection:
    def __init__(self, diffuse_part: float, specular_part: float, cosine_exponent: float, 
                 enable_sojourn_time: bool, sojourn_freq: float, sojourn_e: float):
        self.diffuse_part = diffuse_part # 0..1
        self.specular_part = specular_part # 0..1
        self.cosine_exponent = cosine_exponent # >= 0
        self.enable_sojourn_time = enable_sojourn_time # True or False
        self.sojourn_freq = sojourn_freq # Hz
        self.sojourn_e = sojourn_e # eV

    def __repr__(self):
        return (f"Reflection(diffuse_part={self.diffuse_part}, specular_part={self.specular_part}, "
                f"cosine_exponent={self.cosine_exponent}, "
                f"enable_sojourn_time={self.enable_sojourn_time}, "
                f"sojourn_freq={self.sojourn_freq} Hz, sojourn_e={self.sojourn_e} eV)")
    
def get_reflection(facet_node: ET.Element) -> Reflection:
    node = get_subnode(facet_node, "Reflection")
    diffuse_part = float(get_node_attribute(node, "diffusePart"))
    specular_part = float(get_node_attribute(node, "specularPart"))
    cosine_exponent = float(get_node_attribute(node, "cosineExponent"))
    enable_sojourn_time = bool(int(get_node_attribute(node, "enableSojournTime")))
    sojourn_freq = float(get_node_attribute(node, "sojournFreq"))
    sojourn_e = float(get_node_attribute(node, "sojournE"))
    return Reflection(diffuse_part, specular_part, cosine_exponent, enable_sojourn_time, sojourn_freq, sojourn_e)

def set_reflection(facet_node: ET.Element, reflection: Reflection) -> None:
    node = get_subnode(facet_node, "Reflection")
    if (abs(1.0-reflection.diffuse_part-reflection.specular_part)>1E-8):
        print("The sum of diffuse and specular reflection parts should be 1.0")
        print(f"Currently diffuse ({reflection.diffuse_part}) plus specular ({reflection.specular_part}) equals {reflection.diffuse_part+reflection.specular_part}")
        return
    set_node_attribute(node, "diffusePart", reflection.diffuse_part)
    set_node_attribute(node, "specularPart", reflection.specular_part)
    set_node_attribute(node, "cosineExponent", reflection.cosine_exponent)
    set_node_attribute(node, "enableSojournTime", int(reflection.enable_sojourn_time))
    set_node_attribute(node, "sojournFreq", reflection.sojourn_freq)
    set_node_attribute(node, "sojournE", reflection.sojourn_e)

# Area (in m2, as opposed to cm2 in Molflow GUI)
def get_facet_area(facet_node):
    area_node = facet_node.find("Area")
    if area_node is not None:
        return float(area_node.attrib['m2'])
    else:
        "Facet has no <Area> tag, only saved with MolFLow 2.9.25+"

# Indices
def get_facet_indices(facet_node: ET.Element) -> List[int]:
    # Find the 'Indices' node within the 'Facet'
    indices_node = get_subnode(facet_node,"Indices")

    # Extract 'Indice' elements and get their 'id' attributes
    indices = []
    for indice in indices_node.findall("Indice"):
        vertex_id = int(indice.get("vertex"))
        indices.append(vertex_id)

    return indices

def set_facet_indices(facet_node: ET.Element, indices: List[int]) -> None:

    indices_node = get_subnode(facet_node, "Indices")
    if indices_node is None:
        indices_node = ET.SubElement(facet_node, "Indices", nb=str(len(indices)))
    else:
        # Clear existing 'Indice' elements if 'Indices' node already exists
        indices_node.clear()
        indices_node.set("nb", str(len(indices)))

    # Add new 'Indice' elements with a counter for 'id'
    for idx, vertex in enumerate(indices):
        ET.SubElement(indices_node, "Indice", id=str(idx), vertex=str(vertex))