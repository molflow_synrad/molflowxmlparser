# MolFlow XML file Python interface

This project aims to provide a Python interface to MolFlow-written XML files.

## Prerequisites

* Python 3 (tested on Python 3.12.4 in September 2024)
* No Python packages are required (`xml.etree.ElementTree` is part of the default Python installation)

## Usage

* Clone the repo to a local folder

### As Python script


* Run `main.py` to try out if the package works. It loads an example file, and calls `run_demo` from `demo.py` to demonstrate most commands. Results are written to command line.
* Now modify `main.py` by changing default content:
  * Specify your custom `input_fileName` variable
  * Optionally also specify `output_fileName` and set `write_result` to `True` to save the modified file
  * Change the *---Do your stuff here---* section to what you actually want to do (by default it calls the demo)

### As Jupyter notebook

* Open `demo_notebook.ipynb` in a Jupyter environment (for example Visual Studio Code)

## Current state

As a proof-of-concept, it currently allows to...

* Read and parse a MolFlow XML file
  * If in .zip format, extract it first, with no intermediate files
* Read and modify the properties on a facet:
  * Indices
  * Sticking factor
  * Opacity
  * Outgassing rate, type
  * Temperature
  * Area (read-only)
  * Reflection
  * Sojourn time
  * Structure and link destination
  * Indices
* Read, modify and create vertices
* Read and modify gas mass
* Read and modify scattering (background gas) properties
* Calculate facet results:
  * Pressure
  * Density
  * Impingement rate
* Calculate texture results:
  * Cell area
  * Hits
  * Impingement rate
  * Pressure
  * Density
* Write the modified XML file back to disk
  * Create destination directory if doesn't exist
  * If in .zip format, compress an xml

## To do

* Read/write more global simulation settings
* Read/write selection groups
* Read global results
* Read profiles
* Read histograms