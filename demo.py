from xmlParse import *

def run_demo(root):
    # Facet count
    facet_count = get_facet_count(root)
    print(f"The geometry has {facet_count} facets.")

    # Get coordinates of a vertex
    vertex_id = 9
    vertex_9 = get_vertex(root, vertex_id)
    z_coord = vertex_9.z
    print(f"The Z coordinate of vertex {vertex_id} is {z_coord}")

    # Or a one-liner to get vertex 2's y coord:
    y_coord_2 = get_vertex(root, 2).y
    print(f"The Y coordinate of vertex 2 is {y_coord_2}")

    # Change vertex 2's y coord to 1.1cm
    set_vertex_y(root, 2, 1.1)
    y_coord_2 = get_vertex(root, 2).y
    print(f"The Y coordinate of vertex 2 is now {y_coord_2}")

    # Or update vertex 9 altogether with a new one
    new_vertex_9 = Vertex(1.5, 2.5, 3.5)
    set_vertex(root, 9, new_vertex_9)
    vertex_9 = get_vertex(root, 9)
    print(f"Vertex 9 updated to: {vertex_9}")  # Has its own repr function

    # Add a new vertex (as last)
    last_vertex = Vertex(10.0,10.0,20.0)
    add_vertex(root,last_vertex)

    last_vertex_id = get_vertex_count(root)-1    
    print(f"Added vertex id {last_vertex_id}: {get_vertex(root,last_vertex_id)}")

    # Change the scattering mass ratio to 1.5
    scatteringProperties = get_scattering_properties(root) #get current properties
    print(f"Old mass ratio: {scatteringProperties.massRatio}")
    scatteringProperties.massRatio = 1.5 #change one parameter
    set_scattering_properties(root,scatteringProperties) #set modified properties
    # Verify
    scatteringProperties = get_scattering_properties(root) #get current properties
    print(f"New mass ratio: {scatteringProperties.massRatio}")

    # Get the properties of facet id 3
    facet_id = 3  # Indexed from 0
    facet_node = find_facet_node(root, facet_id)

    # Update the sticking on facet to 0.5
    print(f"Sticking on facet id {facet_id} was {get_sticking(facet_node)}")
    set_sticking(facet_node, 0.5)
    print(f"Sticking on facet id {facet_id} is now {get_sticking(facet_node)}")

    # Update the opacity on facet to 0 (transparent)
    print(f"Opacity on facet id {facet_id} was {get_opacity(facet_node)}")
    set_opacity(facet_node, 0)
    print(f"Opacity on facet id {facet_id} is now {get_opacity(facet_node)}")

    # Set the facet to 2-sided
    print(f"Facet id {facet_id} two-sided: {is_2_sided(facet_node)}")
    set_2_sided(facet_node,True)
    print(f"Facet id {facet_id} two-sided: {is_2_sided(facet_node)}")

    # Update the outgassing rate on facet
    print(f"Facet id {facet_id} outgassing (old): {get_outgassing(facet_node)}")
    set_outgassing_rate(facet_node, 10.0) # Simply change rate
    print(f"Facet id {facet_id} outgassing (new): {get_outgassing(facet_node)}")

    # Actually enable outgassing (as Cosine):
    outgassing = get_outgassing(facet_node)
    outgassing.type = DesorptionType.Cosine
    set_outgassing(facet_node,outgassing)
    print(f"Facet id {facet_id} outgassing (new): {get_outgassing(facet_node)}")

    # Update the temperature on facet to 400K
    print(f"Temp on facet id {facet_id} was {get_temperature(facet_node)} K")
    set_temperature(facet_node, 400.0)
    print(f"Temp on facet id {facet_id} is now {get_temperature(facet_node)} K")

    # Reflection
    facet_reflection = get_reflection(facet_node)
    print(f"Facet reflection:\n{facet_reflection}")
    # Update to mirror reflection
    facet_reflection.diffuse_part=0.0
    facet_reflection.specular_part=1.0
    set_reflection(facet_node,facet_reflection)
    facet_reflection = get_reflection(facet_node)
    print(f"Updated facet reflection:\n{facet_reflection}")

    # Get structure and link
    print(f"Facet is in structure id {get_structure(facet_node)} and link target is {get_link(facet_node)}")

    # Print facet area
    area = get_facet_area(facet_node)
    print(f"Area of facet id {facet_id} is {area} m2")

    # Access vertex index
    indices = get_facet_indices(facet_node)
    print(f"Facet {facet_id} indices: {indices}")

    # Set new indices
    new_indices = [1, 2, 3]
    set_facet_indices(facet_node, new_indices)

    indices = get_facet_indices(facet_node)
    print(f"Updated facet {facet_id} indices: {indices}")

    # Results
    moment_id = 0  # Constant flow

    # Print pressure on facet at moment 0 (const. flow)
    print(f"Pressure on facet id {facet_id} is {get_facet_pressure(root, facet_id, moment_id)} mbar")

    # Density
    print(f"Density on facet id {facet_id} is {get_facet_density(root, facet_id, moment_id)} molecules/m3")

    # Impingement rate

    print(f"Impingement rate on facet id {facet_id} is {get_facet_impingement(root, facet_id, moment_id)} hits/m2/s")

    # Texture

    rowId = 2
    colId = 3

    # Helper quantities that are reused
    gas_mass = get_gas_mass(root)
    molecules_per_tp = get_molecules_per_tp(root,moment_id)
    
    ## MC hits

    hit_texture = get_hits_texture(root, facet_id, moment_id)
    cell_hits = hit_texture[rowId][colId]
    print(f"Hits on facet id {facet_id} at moment {moment_id} at index [{rowId},{colId}]: {cell_hits}")

    ## Cell area

    area_texture = get_area_texture(root,facet_id)
    cell_area=area_texture[rowId][colId]
    print(f"Texture cell area on facet id {facet_id} at index [{rowId},{colId}]: {cell_area} m2")

    ## Impingement rate
    
    impingement_on_cell = calculate_cell_impingement(molecules_per_tp,cell_hits,cell_area)
    print(f"Impingement rate on facet id {facet_id} at moment {moment_id} at index [{rowId},{colId}]: {impingement_on_cell} hits/m2/s")

    ## Pressure

    sum_v_ort_per_area_texture = get_sum_v_ort_texture(root,facet_id,moment_id)
    cell_sum_v_ort_per_area_value = sum_v_ort_per_area_texture[rowId][colId]
    pressure_on_cell = calculate_cell_pressure(gas_mass,molecules_per_tp,cell_sum_v_ort_per_area_value)
    print(f"Pressure on facet id {facet_id} at moment {moment_id} at index [{rowId},{colId}]: {pressure_on_cell} mbar")

    ## Density

    sum_1_per_v_texture = get_sum_1_per_v_texture(root,facet_id,moment_id)
    sum_1_per_v_cell_value = sum_1_per_v_texture[rowId][colId]
    density_on_cell = calculate_cell_density(molecules_per_tp,sum_1_per_v_cell_value,cell_area)
    print(f"Density on facet id {facet_id} at moment {moment_id} at index [{rowId},{colId}]: {density_on_cell} molecules/m3")





