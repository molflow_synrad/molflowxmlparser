import xml.etree.ElementTree as ET # Python XML parser

class Vertex:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        return f"Vertex(x={self.x}, y={self.y}, z={self.z})"

def find_vertex_node(root,vertex_id):
        # Find the vertex using XPath
    vertex_node = root.find(f"./Geometry/Vertices/Vertex[@id='{vertex_id}']")
    
    if vertex_node is not None:
        return vertex_node
    else:
        print("Vertex id {} not found.".format(vertex_id))
        return None   

def get_vertex(root, vertex_id):
    
    vertex_node = find_vertex_node(root,vertex_id)
    
    if vertex_node is not None:
        # Extract the x, y, z attributes
        x = float(vertex_node.get('x'))
        y = float(vertex_node.get('y'))
        z = float(vertex_node.get('z'))
        
        # Create and return the Vertex object
        return Vertex( x, y, z)
    else:
        print("Vertex is {} not found.".format(vertex_id))
        return None
    
def set_vertex_x(root,vertex_id,new_coord):
    vertex_node = find_vertex_node(root,vertex_id)
    vertex_node.set('x', str(new_coord)) 

def set_vertex_y(root,vertex_id,new_coord):
    vertex_node = find_vertex_node(root,vertex_id)
    vertex_node.set('y', str(new_coord)) 

def set_vertex_z(root,vertex_id,new_coord):
    vertex_node = find_vertex_node(root,vertex_id)
    vertex_node.set('z', str(new_coord)) 

def set_vertex(root, vertex_id, new_vertex):

    vertex_node = find_vertex_node(root,vertex_id)  

    # Update the x, y, z attributes with the values from the Vertex object
    vertex_node.set('x', str(new_vertex.x))
    vertex_node.set('y', str(new_vertex.y))
    vertex_node.set('z', str(new_vertex.z))

def add_vertex(root, new_vertex):
    # Find the Vertices node
    vertices_node = root.find("./Geometry/Vertices")
    
    if vertices_node is None:
        print("Vertices node not found.")
        return None
    
    # Determine the next available id by counting existing Vertex elements
    vertex_count = get_vertex_count(root)
    new_id = str(vertex_count)
    
    # Create a new Vertex element
    vertex_element = ET.Element('Vertex')
    
    # Set the auto-assigned id, x, y, z attributes
    vertex_element.set('id', new_id)
    vertex_element.set('x', str(new_vertex.x))
    vertex_element.set('y', str(new_vertex.y))
    vertex_element.set('z', str(new_vertex.z))
    
    # Append the new vertex to the Vertices node
    vertices_node.append(vertex_element)
    return vertex_element

def get_vertex_count(root):
    # Find the Vertices node
    vertices_node = root.find("./Geometry/Vertices")
    
    if vertices_node is None:
        print("Vertices node not found.")
        return None
    
    # Determine the next available id by counting existing Vertex elements
    vertex_count = len(vertices_node.findall("Vertex"))
    return vertex_count