# Textures are stored as tabbed text in a CDATA Xml element.
# This converts them to a table with [row][col] index access
def tabbed_text_to_table(cdata_text, cast_type=float):
    """
    Convert a tabbed text (as CDATA) to a table (list of lists).
    
    Parameters:
    - cdata_text: The tabbed text to convert.
    - cast_type: The type to cast each value to (default is float).
    
    Returns:
    - A 2D list of the converted values.
    """
    # Split the CDATA content into rows based on newline characters
    rows = cdata_text.strip().split('\n')

    # Initialize a 2D list to hold parsed values
    table = []
    for row in rows:
        # Split each row into columns based on tabs
        columns = row.strip().split('\t')

        # Convert each column value to the specified type (int, float, etc.)
        row_values = [cast_type(col) for col in columns]
        table.append(row_values)
    return table

# Gets a subnode of a node using xpath
def get_subnode(node, node_relative_path):
    if node is not None:
        subnode = node.find(node_relative_path)

        if subnode is None:
            print('Node with XML Path "{}" not found.'.format(node_relative_path))
        else:
            return subnode

# Gets a node's attribute by name
# Returns a string, convert to float if necessary
def get_node_attribute(node, attribute_name):
    if attribute_name in node.attrib:
        return node.attrib[attribute_name]
    else:
        print("The '{}' attribute was not found.".format(attribute_name))

# Sets a node's attribute by name
def set_node_attribute(node, attribute_name, new_value):
    if attribute_name in node.attrib:
        node.set(attribute_name,str(new_value))
    else:
        print("The '{}' attribute was not found.".format(attribute_name))