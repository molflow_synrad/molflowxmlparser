import xml.etree.ElementTree as ET
from xmlHelper import *
from simulationProperties import *
from facetProperties import *

# Results

## Textures

### Number of Monte Carlo hits texture
def get_hits_texture(root, facet_id, moment_id):
    # Define the XPath expression to find the Facet result element
    xpath_expr = "./MolflowResults/Moments/Moment[@id='{}']/FacetResults/Facet[@id='{}']".format(moment_id,facet_id)

    # Find the matching Facet result element
    facet_elem = root.find(xpath_expr)

    if facet_elem is not None:
        # Find the <Texture> element
        texture_elem = facet_elem.find('Texture')

        if texture_elem is not None:
            # Find the <count> element inside <Texture>
            count_elem = texture_elem.find('count')

            if count_elem is not None:
                # Access and parse the content of <count>
                count_texture_text = count_elem.text
                table = tabbed_text_to_table(count_texture_text, cast_type=int)
                return table
            else:
                print('<count> element not found in Texture.')
        else:
            print('<Texture> element not found in Facet result.')
    else:
        print("Facet result element with facet_id='{}' and moment_id='{}' not found in XML.".format(facet_id,moment_id))

### Sum_1_per_v_ort_per_area texture
### Similar to sum_1_per_v_ort counter on facets, but already normalized by cell area (which we don't know)
def get_sum_1_per_v_texture(root, facet_id, moment_id):
    # Define the XPath expression to find the Facet result element
    xpath_expr = "./MolflowResults/Moments/Moment[@id='{}']/FacetResults/Facet[@id='{}']".format(moment_id,facet_id)

    # Find the matching Facet result element
    facet_elem = root.find(xpath_expr)

    if facet_elem is not None:
        # Find the <Texture> element
        texture_elem = facet_elem.find('Texture')

        if texture_elem is not None:
            # Find the <count> element inside <Texture>
            sum_1_per_v_elem = texture_elem.find('sum_1_per_v')

            if sum_1_per_v_elem is not None:
                # Access and parse the content of <count>
                sum_1_per_v_texture_text = sum_1_per_v_elem.text
                table = tabbed_text_to_table(sum_1_per_v_texture_text,cast_type=float)
                return table
            else:
                print('<sum_1_per_v> element not found in Texture.')
        else:
            print('<Texture> element not found in Facet result.')
    else:
        print("Facet result element with facet_id='{}' and moment_id='{}' not found in XML.".format(facet_id,moment_id))

### Sum_v_ort_per_area texture
### Similar to sum_v_ort counter on facets, but already normalized by cell area (which we don't know)
def get_sum_v_ort_texture(root, facet_id, moment_id):
    # Define the XPath expression to find the Facet result element
    xpath_expr = "./MolflowResults/Moments/Moment[@id='{}']/FacetResults/Facet[@id='{}']".format(moment_id,facet_id)

    # Find the matching Facet result element
    facet_elem = root.find(xpath_expr)

    if facet_elem is not None:
        # Find the <Texture> element
        texture_elem = facet_elem.find('Texture')

        if texture_elem is not None:
            # Find the <count> element inside <Texture>
            sum_v_ort_elem = texture_elem.find('sum_v_ort')

            if sum_v_ort_elem is not None:
                # Access and parse the content of <count>
                sum_v_ort_texture_text = sum_v_ort_elem.text
                table = tabbed_text_to_table(sum_v_ort_texture_text,cast_type=float)
                return table
            else:
                print('<sum_v_ort> element not found in Texture.')
        else:
            print('<Texture> element not found in Facet result.')
    else:
        print("Facet result element with facet_id='{}' and moment_id='{}' not found in XML.".format(facet_id,moment_id))

### Sum_v_ort_per_area texture
### Similar to sum_v_ort counter on facets, but already normalized by cell area (which we don't know)
def get_area_texture(root, facet_id):
    # Define the XPath expression to find the Facet result element
    xpath_expr = "./MolflowResults/Moments/Moment[@id='{}']/FacetResults/Facet[@id='{}']".format(0,facet_id) #Area always stored in moment 0

    # Find the matching Facet result element
    facet_elem = root.find(xpath_expr)

    if facet_elem is not None:
        # Find the <Texture> element
        texture_elem = facet_elem.find('Texture')

        if texture_elem is not None:
            # Find the <count> element inside <Texture>
            area_elem = texture_elem.find('cell_area_m2')

            if area_elem is not None:
                # Access and parse the content of <count>
                area_elem_text = area_elem.text
                table = tabbed_text_to_table(area_elem_text,cast_type=float)
                return table
            else:
                print('<cell_area_m2> element not found in Texture. It is only saved with MolFlow 2.9.26 and later.')
        else:
            print('<Texture> element not found in Facet result.')
    else:
        print("Facet result element with facet_id='{}' and moment_id='{}' not found in XML.".format(facet_id,0))

def calculate_cell_pressure(gas_mass,molecules_per_tp,sum_v_ort_per_area):
    pa_to_mbar = 0.01
    kg_to_g = 1000.0
    m2_to_cm2 = 1.0E4
    pressure = sum_v_ort_per_area * m2_to_cm2 * gas_mass / kg_to_g / 6E23 * pa_to_mbar * molecules_per_tp
    return pressure

def calculate_cell_density(molecules_per_tp,sum_1_per_v_ort,cell_area):
    density = sum_1_per_v_ort / cell_area * molecules_per_tp
    return density

def calculate_cell_impingement(molecules_per_tp,cell_hits,cell_area):
    imp_rate = cell_hits / cell_area * molecules_per_tp
    return imp_rate

# Facet results

def get_facet_pressure(root,facetId,moment_id):
    gas_mass = get_gas_mass(root)
    facet_area = get_facet_area(find_facet_node(root,facetId))
    molecules_per_tp = get_molecules_per_tp(root,moment_id)
    pa_to_mbar = 0.01
    kg_to_g = 1000.0
    dCoef = molecules_per_tp * gas_mass / kg_to_g / 6E23 * pa_to_mbar
    sum_v_ort = float(get_facet_hit_result_node(root,facetId,moment_id).attrib['sum_v_ort'])
    return sum_v_ort * dCoef / facet_area

def get_facet_density(root,facetId,moment_id):
    facet_area = get_facet_area(find_facet_node(root,facetId))
    molecules_per_tp = get_molecules_per_tp(root,moment_id)
    dCoef = molecules_per_tp;
    sum_1_per_ort_velocity = float(get_facet_hit_result_node(root,facetId,moment_id).attrib['sum_1_per_v'])
    return sum_1_per_ort_velocity * dCoef / facet_area

def get_facet_impingement(root,facetId,moment_id):
    facet_area = get_facet_area(find_facet_node(root,facetId))
    molecules_per_tp = get_molecules_per_tp(root,moment_id)
    dCoef = molecules_per_tp;
    nbHitEquiv = float(get_facet_hit_result_node(root,facetId,moment_id).attrib['nbHitEquiv'])
    return nbHitEquiv * dCoef / facet_area

def get_molecules_per_tp(root,moment_id):
    moment_node = get_moment_result_node(root,moment_id)
    if 'molecules_per_TP' in moment_node.attrib:
        return float(moment_node.attrib['molecules_per_TP'])
    else:
        print("The 'molecules_per_TP' attribute was not found. It's only saved with MolFlow 2.9.25+")

def get_facet_hit_result_node(root,facetId,moment_id):
    moment_node = get_moment_result_node(root,moment_id)
    facetresult_node = get_facet_result_node(moment_node,facetId)
    hit_node = facetresult_node.find("Hits")
    if hit_node is not None:
        return hit_node
    else:
        "Facet result node has no <Hits> tag"

# Helper
def get_moment_result_node(root,moment_id):
    xpath_expr = "./MolflowResults/Moments/Moment[@id='{}']".format(moment_id)
    # Find the matching Facet result element
    moment_node = root.find(xpath_expr)

    if moment_node is not None:
        return moment_node
    else:
        print("Moment {} result not found".format(moment_id))

# Helper
def get_facet_result_node(moment_node,facetId):
    xpath_expr = "./FacetResults/Facet[@id='{}']".format(facetId)
    # Find the matching Facet result element
    facetresult_node = moment_node.find(xpath_expr)

    if facetresult_node is not None:
        return facetresult_node
    else:
        print("Facet {} result not found".format(facetId))