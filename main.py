from xmlParse import *
from fileIO import *
from demo import *

# Molflow XML parser example
# Works with examples/quickpipe.xml included in the repo
# Author: Marton ADY
# Copyright: CERN (2024)

# Input and output file paths
input_fileName = "examples/quickpipe.xml" # .xml or .zip
output_fileName = "out/output.zip" # .xml or .zip
write_result = False # save loaded file at script end

def main():

    # Open MolFlow file
    tree = open_xml_or_zip(input_fileName)
    root = tree.getroot()

# --------- DO YOUR STUFF HERE ----------------------------------------------

    run_demo(root)

# ----------------------------------------------------------------------------
        
    
    # Save modified file (optional)
    if write_result:
        write_xml_or_zip(tree, output_fileName)




if __name__ == "__main__":
    main()
