import xml.etree.ElementTree as ET
import zipfile
import io
import os

# Opens an XML file either directly or by extracting the zip, returns the XML tree
# Tree to root element example: root = tree.getroot()
def open_xml_or_zip(xml_file_name):
    if xml_file_name.endswith('.zip'):
        # Extract XML file from ZIP
        with zipfile.ZipFile(xml_file_name, 'r') as zip_ref:
            # Assuming there's only one XML file in the ZIP archive
            xml_file_names = [name for name in zip_ref.namelist() if name.endswith('.xml')]
            if not xml_file_names:
                raise ValueError("No XML files found in the ZIP archive.")
            xml_content = zip_ref.read(xml_file_names[0])
    else:
        # Read XML file directly
        with open(xml_file_name, 'rb') as f:
            xml_content = f.read()

    # Parse XML content and return ElementTree
    tree = ET.ElementTree(ET.fromstring(xml_content))
    return tree

# Writes a tree to an output file, either XML or a ZIP-compressed XML
def write_xml_or_zip(tree, output_fileName):
    
    create_dir_if_doesnt_exist(output_fileName)

    if output_fileName.endswith('.xml'):
        # Write as regular XML file
        tree.write(output_fileName, encoding='utf-8', xml_declaration=True)
        print(f"XML content written to {output_fileName}")
    elif output_fileName.endswith('.zip'):
        # Write as ZIP file containing XML
        with zipfile.ZipFile(output_fileName, 'w') as zipf:
            zipf.writestr(os.path.basename(output_fileName.replace('.zip', '.xml')), ET.tostring(tree.getroot(), encoding='utf-8'))
        print(f"XML content written to {output_fileName}")
    else:
        raise ValueError("Output file name must end with .xml or .zip")

# Makes sure that the output file path has the directory created    
def create_dir_if_doesnt_exist(output_fileName):
    output_dir = os.path.dirname(output_fileName)
    if output_dir and not os.path.exists(output_dir):
        os.makedirs(output_dir)